var Map = (function () {
    var proto = function (game, width, height) {
        this._game = game;
        this._width = width;
        this._height = height;
        this._places = null;
        this._$view = this._createView();

        this._generatePlaces();
        this._refreshView();
    };

    proto.prototype = {
        getPlace: function (x, y) {
            return this._places[x] && this._places[x][y] ? this._places[x][y] : null;
        },
        getView: function () {
            return this._$view;
        },
        randomPlacePlayer: function (player) {
            var place = this._findRandomFreePlace();
            place.setPlayer(player);
        },
        randomPlaceWeapon: function (weapon) {
            var place = this._findRandomFreePlace();
            place.setWeapon(weapon);
        },
        movePlayer: function (player, dir) {
            var place = this._findPlayerPlace(player);
            var newPlace = null;

            switch(dir) {
                case 'down':
                    newPlace = this.getPlace(place.getX(), place.getY() + 1);
                    break;
                case 'up':
                    newPlace = this.getPlace(place.getX(), place.getY() - 1);
                    break;
                case 'left':
                    newPlace = this.getPlace(place.getX() - 1, place.getY());
                    break;
                case 'right':
                    newPlace = this.getPlace(place.getX() + 1, place.getY());
                    break;
            }

            if(newPlace) {
                if(newPlace.getPlayer()) {
                    console.info('todo: fight');
                } else if(newPlace.getType() === 1) {
                    // switch players
                    place.setPlayer(null);
                    newPlace.setPlayer(player);

                    var placeWeapon = newPlace.getWeapon();

                    if(placeWeapon) {
                        var playerWeapon = player.getWeapon();
                        player.setWeapon(null);
                        newPlace.setWeapon(null);

                        player.setWeapon(placeWeapon);
                        newPlace.setWeapon(playerWeapon);
                    }
                }
            }
        },
        _createView: function () {
            return $('<div data-game-view="Map"></div>');
        },
        _findPlayerPlace: function (player) {
            for (var x = 0; x < this._places.length; x++) {
                for (var y = 0; y < this._places.length; y++) {
                    var place = this.getPlace(x, y);
                    if (player === this.getPlace(x, y).getPlayer())
                        return place;
                }
            }
        },
        _findRandomFreePlace: function () {
            var freePlaces = [];

            for (var x = 0; x < this._places.length; x++) {
                for (var y = 0; y < this._places.length; y++) {
                    var place = this.getPlace(x, y);
                    if (place.getType() === 1 && !place.getPlayer() && !place.getWeapon())
                        freePlaces.push(place);
                }
            }

            var index = Utils.getRandomInt(0, freePlaces.length);

            return freePlaces[index];
        },
        _generatePlaces: function () {
            this._places = [];

            for (var x = 0; x < this._width; x++) {
                this._places[x] = [];
                for (var y = 0; y < this._height; y++) {
                    var type = Utils.getRandomIntInclusive(1, 2);
                    this._places[x][y] = new MapPlace(x, y, type);
                }
            }
        },
        _refreshView: function () {
            for (var y = 0; y < this._places.length; y++) {
                for (var x = 0; x < this._places.length; x++) {
                    this._$view.append(this.getPlace(x, y).getView());
                }
                this._$view.append('<div style="clear:both"></div>');
            }
        }
    };

    return proto;
})();

