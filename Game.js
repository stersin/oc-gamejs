var Game = (function () {

    var proto = function () {
        this._registerEvents();
        this._$view = this._createView();
        this._players = [];
        this._arena = null;
    };

    proto.prototype = {
        endTurn: function(player) {
            this._currentPlayerIndex++;

            if(this._currentPlayerIndex >= this._players.length ) {
                this._currentPlayerIndex = 0;
            }
        },
        fight: function(player, againstPlayer) {
            this._arena([player, againstPlayer]);
            this.getView().append(this._arena.getView());
        },
        getCurrentPlayer: function() {
            return this._players[this._currentPlayerIndex];
        },
        getMap: function() {
            return this._map;
        },
        getView: function() {
            return this._$view;
        },
        startNew: function() {
            this._players = [
                new Player(game, 'p1'),
                new Player(game, 'p2')
            ];

            this._map = new Map(game, 10, 10);

            for(var i in this._players) {
                this._map.randomPlacePlayer(this._players[i]);
            }

            for(var i = 0; i < 2; i++) {
                this._map.randomPlaceWeapon(new Weapon('sword', 10));
            }

            for(var i = 0; i < 2; i++) {
                this._map.randomPlaceWeapon(new Weapon('axe', 20));
            }

            this._currentPlayerIndex = 0;

            this._refreshView();
            $('body').append(this.getView());
        },
        _createView: function() {
            return $('<div data-game-view="Game"></div>')
        },
        _refreshView: function() {
            this._$view.append(this._map.getView());
        },
        _registerEvents: function() {
            var _this = this;
            $(document).on('keydown', function(e){
                if(_this.getCurrentPlayer().dispatchKeyDownEvent(e)) {
                    e.preventDefault();
                }
            });
        }
    };

    return proto;
})();