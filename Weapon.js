var Weapon = (function(){
    var proto = function(type, dmg) {
        this._type = type;
        this._dmg = dmg;

        this._$view = this._createView();
    };

    proto.prototype = {
        getView: function() {
            return this._$view;
        },
        _createView: function () {
            return $('<div data-game-view="Weapon">' + this._type + '</div>');
        }
    };

    return proto;
})();