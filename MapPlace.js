var MapPlace = (function () {
    var proto = function (x, y, type) {
        this._x = x;
        this._y = y;
        this._type = type;
        this._$view = this._createView();
        this._weapon = null;
        this._player = null;
    };

    proto.prototype = {
        getX: function() {
            return this._x;
        },
        getY: function() {
            return this._y;
        },
        getPlayer: function() {
            return this._player;
        },
        getType: function() {
            return this._type;
        },
        getView: function() {
            return this._$view;
        },
        getWeapon: function() {
            return this._weapon;
        },
        setPlayer: function(player) {
            this._player = player;

            this._refreshView();
        },
        setWeapon: function(weapon) {
            this._weapon = weapon;

            this._refreshView();
        },
        _createView: function() {
            return $('<div data-game-view="MapPlace" data-game-map-place-type="' + this._type + '"></div>');
        },
        _refreshView: function() {
            this._$view.html('');

            if(this._weapon) {
                this._$view.append(this._weapon.getView());
            }

            if(this._player) {
                this._$view.append(this._player.getView());
            }
        }
    };

    return proto;
})();


