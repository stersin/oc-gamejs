var Player = (function () {
    var proto = function (game, name) {
        this._game = game;
        this._name = name;
        this._$view = this._createView();
        this._weapon = null;

        this._weapon = new Weapon('fist', 10);
    };

    proto.prototype = {
        getView: function() {
            return this._$view;
        },
        getWeapon: function() {
            return this._weapon;
        },
        setWeapon: function (weapon) {
            this._weapon = weapon;

            this._refreshView();
        },
        _createView: function () {
            return $('<div data-game-view="Player">' + this._name + '</div>');
        },
        _refreshView: function() {

        },
        dispatchKeyDownEvent: function(e) {
            switch(e.which) {
                case 13:
                    this._game.endTurn(this);
                    break;

                case 37: // left
                    this._game.getMap().movePlayer(this, 'left');
                    break;

                case 38: // up
                    this._game.getMap().movePlayer(this, 'up');
                    break;

                case 39: // right
                    this._game.getMap().movePlayer(this, 'right');
                    break;

                case 40: // down
                    this._game.getMap().movePlayer(this, 'down');
                    break;

                default:
                    return false; // exit this handler for other keys
            }

            return true;
        }
    };

    return proto;
})();


