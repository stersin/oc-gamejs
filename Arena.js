var Arena = (function(){
    var proto = function(players) {
        this._$view = this._createView();
        this._players = players;
    };

    proto.prototype = {
        destroy: function() {
            this._$view.remove();
        },
        getView: function() {
            return this._$view;
        },
        _createView: function () {
            return $('<div data-game-view="Arean"></div>');
        }
    };

    return proto;
})();